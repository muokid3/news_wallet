<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleView;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{

    public function index()
    {
        return Article::paginate(10);
    }

    public function show(Article $article)
    {
        $articleView = new ArticleView();
        $articleView->article_id = $article->id;
        $articleView->saveOrFail();

        return $article;
    }

    public function category_articles(Category $category)
    {
        return $category->articles()->paginate(10);
    }

    public function store(Request $request)
    {
        $article = new Article();
        $article->user_id = $request->user()->id;
        $article->category_id = $request->category_id;
        $article->link = $request->article_link;
        $article->title = $request->title;
        $article->featured_image = $request->featured_image_link;
        $article->author = $request->author;
        $article->website = $request->website;
        $article->meta_description = $request->meta_description;
        $article->saveOrFail();

        return response()->json([
            'message' => 'Article saved',
            'article' => $article
        ], 201);
    }

    public function update(Request $request, Article $article)
    {
        $article->category_id = $request->category_id;
        $article->link = $request->article_link;
        $article->title = $request->title;
        $article->featured_image = $request->featured_image_link;
        $article->author = $request->author;
        $article->website = $request->website;
        $article->meta_description = $request->meta_description;
        $article->update();

        return response()->json($article, 200);
    }

    public function delete(Article $article)
    {
        $article->delete();

        return response()->json(null, 204);
    }
}
