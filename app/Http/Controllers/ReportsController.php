<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{
    public function article_views(Article $article)
    {
        return response()->json([
            'article_id' => $article->id,
            'article_title' => $article->title,
            'views' => $article->views->count(),
        ], 200);
    }


    public function popular_articles()
    {
        return response()->json(Article::all('id','title')->sortByDesc('views'), 200);

    }

    public function popular_categories()
    {
        return response()->json(Category::all('id','category_name')->sortByDesc('articles'), 200);

    }
}
