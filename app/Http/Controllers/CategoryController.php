<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        return Category::all();
    }

    public function show(Category $category)
    {
        return $category;
    }

    public function store(Request $request)
    {
        $category = new Category();
        $category->user_id = $request->user()->id;
        $category->category_name = $request->name;

        $category->saveOrFail();

        return response()->json([
            'message' => 'Category saved',
            'category' => $category
        ], 201);
    }

    public function update(Request $request, Category $category)
    {
        $category->category_name = $request->name;

        $category->update();

        return response()->json($category, 200);
    }

    public function delete(Category $category)
    {
        $category->delete();

        return response()->json(null, 204);
    }
}
