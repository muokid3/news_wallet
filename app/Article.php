<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public function views()
    {
        return $this->hasMany('App\ArticleView', 'article_id');
    }

    public function toArray() {
        $data = parent::toArray();
        $data['views'] = $this->views->count();
        return $data;
    }

}
