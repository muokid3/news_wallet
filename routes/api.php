<?php

use Illuminate\Http\Request;
Use App\Article;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/*
 * AUTH APIs:
 * login, signup, logout, user
 * */
Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});



/*
 * Private APIs :
 *  CREATE, UPDATE, and DELETE
 * */
Route::group([
    'middleware' => 'auth:api'
], function() {
    Route::post('articles', 'ArticleController@store');
    Route::put('articles/{article}', 'ArticleController@update');
    Route::delete('articles/{article}', 'ArticleController@delete');


    Route::post('categories', 'CategoryController@store');
    Route::put('categories/{category}', 'CategoryController@update');
    Route::delete('categories/{category}', 'CategoryController@delete');
});


/*
 * Public APIs :
 *  READ
 * */
Route::get('articles', 'ArticleController@index');
Route::get('articles/{article}', 'ArticleController@show');
Route::get('articles/category/{category}', 'ArticleController@category_articles');


Route::get('categories', 'CategoryController@index');
Route::get('categories/{category}', 'CategoryController@show');


Route::group([
    'prefix' => 'reports'
], function () {
    Route::get('articles/views/{article}', 'ReportsController@article_views');
    Route::get('articles/popular', 'ReportsController@popular_articles');
    Route::get('categories/popular', 'ReportsController@popular_categories');

});


