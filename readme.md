<p align="center">Built with <img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Getting Started


- Clone the repo 
- Run `composer install`
- Copy `.env.example` and rename it to `.env`
- Replace the database credentials appropriately then save the file
- Run `php artisan config:cache`
- Run `php artisan migrate`
- Run `php artisan passport:install`
- Run `php artisan serve` 
- Load the Postman APIs, start with registtrations and follow on the other APIs as documented on postman
- 
- Have fun creating and managing your articles :)
